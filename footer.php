<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Footer.PHP file</title>

        <link rel="stylesheet" type="text/css" href="css/estilo2.css">
		<link rel="script" type="text/javascript" href="js/script.js">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	</head>

	<body>
        
        <footer class="footer py-3 bg-light text-center align-bottom " >
			<p> Desenvolvido por 
			<a href="https://gitlab.com/leandroms1975/" target= "_blank" rel="noreferrer">Alex Leandro</a> em PHP. </p>
			<p> Banco de Dados PostgreSQL disponível em 
			<a href="https://dashboard.heroku.com/" target= "_blank" rel="noreferrer">Heroku</a> .</p>
		</footer>
	
    </body>
</html>