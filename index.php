<!DOCTYPE html>
<html>
    <head>
        <title>App Lista de Tarefas</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width. initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/estilo2.css">

    </head>
    <body>

        <div id="header">
            <?php include 'header.php' ?>
        </div>

        <div id="nav">
            <section id="page">
                <?php include 'page1.php' ?>
            </section>
        </div>

        <div id="footer">
            <?php include 'footer.php' ?>
        </div>

        <script src="./js/script.js"></script>

    </body>
</html>